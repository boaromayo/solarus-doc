/**
\page lua_api_state Custom states

\tableofcontents

This module provides a datatype \c state
that represents a custom state allowing advanced customization of the
\ref lua_api_hero "hero".

\section lua_api_state_overview Overview

Custom states can be used to precisely control everything that can happen to
the \ref lua_api_hero "hero" in particular situations.

The engine provides a number of built-in states like
\c "free", \c "frozen", \c "hurt", \c "swimming", \c "pushing" and more
(see \ref lua_api_hero "hero:get_state()" for the whole list).
In every state, the hero can or cannot do specific actions (like moving),
and can react or ignore other entities.

Most of the time, you don't need custom states.
Custom states are for advanced users who want more control.
Remember that in any built-in state, you can already customize the essential
features like sprites and movements.
More often that not, the \c "frozen" built-in state is enough to make a lot of
things, like cutscenes.

Sometimes however, you may want more advanced customization.
For instance, if the built-in \c "swimming" state does not fit your needs,
you can make a custom state instead with your own swimming implementation.
Or sometimes, you just want a completely new behavior that is not
covered by any existing built-in state.

\subsection lua_api_state_overview_how_to How to implement a custom state

The \ref lua_api_hero "hero" always has only one current state.
It can either be a built-in state or a custom state.

To make a custom state, first create a custom state object with
\ref lua_api_state_create "sol.state.create()".
Then, use some of the many methods and events documented below,
and finally start your state on the \ref lua_api_hero "hero"
with \ref lua_api_hero_start_state "hero:start_state(state)".

After this operation, the current state string of the hero
(as returned by \ref lua_api_hero "hero:get_state()")
is \c "custom", and no longer a usual built-in state like
\c "free", \c "frozen", \c "swimming", etc.
You can then use
\ref lua_api_hero_get_state_object "hero:get_state_object()" to
retrieve the actual custom state object that you created.

When your state finishes, because you or the engine started another state
(built-in or custom), it is your responsibility to clean things that you may
have done on objects other than the state.
For instance, if you
\ref lua_api_entity_create_sprite "added a sprite" to the hero for your
custom state, maybe you want to remove it when the state finishes.
The event \ref lua_api_state_on_finished "state:on_finished()"
is the appropriate place to do so, because it is called no matter who is
changing the state.

\subsection lua_api_state_overview_example Example

This short example is a state that allows the
\ref lua_api_hero "hero" to move,
but keeps his sprites direction fixed.

\verbatim
local state = sol.state.create()      -- Create a custom state object.
state:set_can_control_movement(true)  -- Set up its properties.
state:set_can_control_direction(false)
hero:start_state(state)               -- Associate it to the hero.
\endverbatim

There are other ways to implement such a feature,
but custom states make this easier.

\section lua_api_state_functions Functions of sol.state

\subsection lua_api_state_create sol.state.create([description])

Creates a custom state but does not start it yet.
- \c description (string, optional): An optional description of your state.
  The engine does nothing special with this description,
  but it may help you distinguish states.
- Return value (state): The custom state created.

\section lua_api_state_methods Methods of the type state

\subsection lua_api_state_get_description state:get_description()

Returns the description of this state.
- Return value (string): The description, or \c nil if no description was set.

\subsection lua_api_state_set_description state:set_description(description)

Sets the description of this state.

The engine does nothing special with this description,
but it may help you distinguish states.
- \c description (string or nil): The description to set,
  or \c nil to set no description.

\subsection lua_api_state_get_entity state:get_entity()

Returns the \ref lua_api_entity "entity" controlled by this state.
- Return value (\ref lua_api_entity "entity"): The entity controlled by this
  state (which can only by the \ref lua_api_hero "hero"),
  or \c nil if the state is not associated to an entity yet.

\remark Even if the state is finished, this method still returns the entity
  that was controlled.

\subsection lua_api_state_get_map state:get_map()

Returns the \ref lua_api_map "map" of the entity controlled by this state.
- Return value (\ref lua_api_map "map"): The map,
  or \c nil if the state is not associated to an entity yet.

\subsection lua_api_state_get_game state:get_game()

Returns the \ref lua_api_game "game" of the entity controlled by this state.
- Return value (\ref lua_api_game "game"): The game,
  or \c nil if the state is not associated to an entity yet.

\subsection lua_api_state_is_started state:is_started()

Returns whether this state is started, that is, if it was activated on an
entity (see \ref lua_api_hero_start_state "hero:start_state()")
and is not finished yet.
- Return value (boolean): \c true if the state is started.

\subsection lua_api_state_is_visible state:is_visible()

Returns whether the entity is visible during this state.
- Return value (boolean): \c true if the entity is visible during this state,
  \c false if it is hidden.

\subsection lua_api_state_set_visible state:set_visible([visible])

Returns whether the entity should be visible during this state.
- \c visible (boolean, optional): \c true to make the entity visible during this state,
  \c false to hide it.
  No value means \c true.

\subsection lua_api_state_get_draw_override state:get_draw_override()

Returns the draw function of this state.

See \ref lua_api_state_set_draw_override "state:set_draw_override()"
for more details.
- Return value (function or nil): The draw function, or \c nil if the draw
  function was not overridden.

\subsection lua_api_state_set_draw_override state:set_draw_override(draw_override)

Changes how this entity is drawn during this state.

You can use this to replace the built-in draw implementation of the engine by
your own function, if the default behavior does not fit your needs.
To do so, your function can either call
\ref lua_api_map_draw_visual "map:draw_visual()"
or draw on \ref lua_api_camera "camera:get_surface()".
- \c draw_override (function or nil): The draw function, or \c nil to
  restore the built-in drawing.
  Your function will receive the following parameters:
  - \c state (state): The custom state of the entity to draw.
  - \c camera (\ref lua_api_camera "camera"): Camera where the entity is drawn.

\remark Even when you set a draw override, events
  \ref lua_api_state_on_pre_draw "state:on_pre_draw()" and
  \ref lua_api_state_on_post_draw "state:on_post_draw()" are still called.

\subsection lua_api_state_get_can_control_direction state:get_can_control_direction()

Returns whether the player controls the direction of entity's sprites during
this state.
- Return value (boolean): \c true if the player controls the sprites direction.

\subsection lua_api_state_set_can_control_direction state:set_can_control_direction(can_control_direction)

Sets whether the player controls the direction of entity's sprites during
this state.

The default value is \c true. If you set this to \c false, then the entity's
sprites no longer automatically take the direction pressed by the player.
- \c can_control_direction (boolean): \c true to let the player control the
  sprites direction.
  No value means \c true.

\subsection lua_api_state_get_can_control_movement state:get_can_control_movement()

Returns whether the player controls the movement of the entity during
this state.
- Return value (boolean): \c true if the player controls the movement.

\subsection lua_api_state_set_can_control_movement state:set_can_control_movement(can_control_movement)

Sets whether the player controls the movement of the entity during
this state.

The default value is \c true.
If this setting is \c true, a movement is automatically created on the entity
and this movement reacts to the player's input.
If you set this to \c false, then the entity has no automatic movement during
this state.
- \c can_control_movement (boolean): \c true to let the player control the
  movement.

\subsection lua_api_custom_state_set_can_traverse state:set_can_traverse([entity_type], traversable)

Sets whether the entity can traverse other entities in this state.

By default, this depends on the other entities: for example,
\ref lua_api_sensor "sensors" can be traversed by default
while \ref lua_api_door "doors" cannot unless they are open.
- \c entity_type (string, optional): A type of entity.
  See \ref lua_api_entity_get_type "entity:get_type()" for the possible values.
  If not specified, the setting will be applied to all entity types for which
  you don't override this setting.
- \c traversable (boolean, function or \c nil): Whether the entity controlled
  by this state can traverse the other entity type. This can be:
  - A boolean: \c true to allow your entity to traverse entities of the
    specified type, \c false otherwise.
  - A function: Custom test.
    This allows you to decide dynamically.
    The function takes your entity (the one controlled by this state)
    and then the other entity as parameters, and should return \c true if you
    allow your entity to traverse the other entity.
    When your entity has a \ref lua_api_movement "movement",
    this function will be called every time it is about to overlap an entity
    of the specified type.
  - \c nil: Clears any previous setting for this entity type and therefore
    restores the default value.

\subsection lua_api_state_get_can_traverse_ground state:get_can_traverse_ground(ground)

Returns whether the entity can traverse the given kind of ground
during this state.
- \c ground (string): A kind of ground.
  See \ref lua_api_map_get_ground "map:get_ground()" for the possible values.
- Return value (boolean): \c true if the ground can be traversed during this
  state.

\subsection lua_api_state_set_can_traverse_ground state:set_can_traverse_ground(ground, traversable)

Sets whether the entity can traverse the given kind of ground
during this state.

By default, this depends on the the ground: for example,
the \c "grass" ground can be traversed by default
while the \c "low wall" ground cannot.
- \c ground (string): A kind of ground.
  See \ref lua_api_map_get_ground "map:get_ground()" for the possible values.
- \c traversable (boolean): Whether the entity can
  traverse this kind of ground during this state.

\subsection lua_api_state_is_gravity_enabled state:is_gravity_enabled()

Returns whether the entity is affected by gravity during this state.

If yes, the entity will fall to the lower layer when the ground below it
is \c "empty".
- Return value (boolean): \c true if the entity is affected by gravity during
  this state.

\subsection lua_api_state_set_gravity_enabled state:set_gravity_enabled(gravity_enabled)

Sets whether the entity is affected by gravity during this state.

If yes, the entity will fall to the lower layer when the ground below it
is \c "empty".

The default value is \c true.
You should typically set this to \c false when the entity is jumping of flying.
- \c gravity_enabled (boolean): \c true to make the entity affected by gravity
  during this state.

\subsection lua_api_state_is_affected_by_ground state:is_affected_by_ground(ground)

Returns whether the given kind of ground affects the entity during this state.
- \c ground (string): A kind of ground.
  See \ref lua_api_map_get_ground "map:get_ground()" for the possible values.
- Return value (boolean): \c true if this ground affects the entity during
  this state.

\subsection lua_api_state_set_affected_by_ground state:set_affected_by_ground(ground, affected)

Sets whether a kind of ground affects the entity during this state.

- \c ground (string): A kind of ground.
  See \ref lua_api_map_get_ground "map:get_ground()" for the possible values.
- \c affected (boolean): \c true to make this ground affect the
  entity during this state.

\subsection lua_api_get_can_come_from_bad_ground state:get_can_come_from_bad_ground()

Returns whether this state remembers the last solid position of the entity as
a place to come back to later if it falls into bad ground like holes or lava.
- Return value (boolean): \c true if solid positions in this state are
  considered as places to come back to when falling into bad grounds.

\subsection lua_api_set_can_come_from_bad_ground state:set_can_come_from_bad_ground()

Sets whether this state remembers the last solid position of the entity as
a place to come back to later if it falls into bad ground like holes or lava.

The default value is \c true.
- \c can_come_from_bad_ground (boolean): \c true if solid positions in this
  state should be considered as places to come back to when falling into bad
  grounds.

\subsection lua_api_state_get_can_be_hurt state:get_can_be_hurt()

Returns whether the entity can be hurt during this state.
- Return value (boolean): \c true if the entity can be hurt during this state.

\remark If you passed a function to
  \ref lua_api_state_set_can_be_hurt "state:set_can_be_hurt()",
  then your function will be evaluated with a \c nil parameter.

\subsection lua_api_state_set_can_be_hurt state:set_can_be_hurt(can_be_hurt)

Sets whether the entity can be hurt during this state.

The default value is \c true.
- \c can_be_hurt (boolean or function): Whether the entity controlled
  by this state can be hurt.
  You can pass a function if you want to decide this depending at the last
  moment, for example depending on the attacker.
  In this case, your function should accept the following parameters
  and return value:
  - \c state (state): The current state itself.
  - \c attacker (\ref lua_api_entity "entity" or nil): the attacker entity,
    or \c nil if the attack does not come from an entity.
  - Return value (boolean): \c true to allow your entity to get hurt by this
    attacker.

\subsection lua_api_state_get_can_use_sword state:get_can_use_sword()

Returns whether the entity can swing the sword during this state.
- Return value (boolean): \c true if the entity can use the sword.

\subsection lua_api_state_set_can_use_sword state:set_can_use_sword(can_use_sword)

Sets whether the entity can swing the sword during this state.

The default value is \c true.
- \c can_use_sword (boolean): \c true to allow to use the sword.

\subsection lua_api_state_get_can_cut state:get_can_cut()

Returns whether the entity can cut another
\ref lua_api_entity "entity"
with the sword during this state.
- Return value (boolean): \c true if the entity can cut a destructible
  entity during this state.

\remark If you passed a function to
  \ref lua_api_state_set_can_cut "state:set_can_cut()",
  then your function will be evaluated with a \c nil parameter.

\subsection lua_api_state_set_can_cut state:set_can_cut(can_cut)

Sets whether the entity can cut another
\ref lua_api_entity "entity"
with the sword during this state.

When the sword sprite overlaps a destructible object that
\ref lua_api_destructible_get_can_be_cut "destructible:get_can_be_cut()"
can be cut, this setting decides if the destructible will actually be cut.

The default value is \c true.
- \c can_cut (boolean or function): Whether the sword can
  cut another entity during this state.
  You can pass a function if you want to decide this at the last moment,
  for example depending on the exact position of the entity about to be cut.
  In this case, your function should support the following parameters
  and return value:
  - \c state (state): The current state itself.
  - \c entity (\ref lua_api_entity "entity" or nil):
    the entity that would be cut,
    or \c nil if there is no entity about to be cut.
  - Return value (boolean): \c true to allow to cut the
    entity with the sword.

\subsection lua_api_state_get_can_use_shield state:get_can_use_shield()

Returns whether the entity can stop attacks with the shield during this state.
- Return value (boolean): \c true if the entity can stop attacks with the
  shield.

\subsection lua_api_state_set_can_use_shield state:set_can_use_shield(can_use_shield)

Sets whether the entity can stop attacks with the shield during this state.

The default value is \c true.
- \c can_use_shield (boolean): \c true if the entity can stop attacks with the
  shield.

\subsection lua_api_state_get_can_use_item state:get_can_use_item([item_id])

Returns whether an \ref lua_api_item "equipment item" can be used
during this state.
- \c item_id (string, optional): Name of the item to test,
  or \c nil to mean items in general.
- Return value (boolean): \c true if the player can use an equipment item
  during this state.

\subsection lua_api_state_set_can_use_item state:set_can_use_item([item_id], can_use_item)

Sets whether an \ref lua_api_item "equipment item" can be used
during this state.

The default value is \c true.
- \c item_id (string, optional): Name of the item to allow or disallow,
  or \c nil to mean items in general.
- \c can_use_item (boolean): \c true to allow the player to use an equipment
  item during this state.

\subsection lua_api_state_get_can_interact state:get_can_interact()

Returns whether the entity can interact with the entities it is facing.
- Return value (boolean): \c true if interactions are allowed in this state.

\subsection lua_api_state_set_can_interact state:set_can_interact(can_interact)

Sets whether the entity can interact with the entities it is facing.

If \c true, when the action game command is pressed while facing an entity
that reacts to interactions
(like an \ref lua_api_npc "NPC"),
then an interaction will occur.

The default value is \c true.
- \c can_interact (boolean): \c true to allow to interact in this state.

\subsection lua_api_state_get_can_grab state:get_can_grab()

Returns whether the entity can grab the obstacles it is facing.
- Return value (boolean): \c true if grab is allowed in this state.

\subsection lua_api_state_set_can_grab state:set_can_grab(can_grab)

Sets whether the entity can start grabbing the obstacles it is facing.

If \c true, the entity will go to state \c "grabbing" when the action command
is pressed while facing an obstacle.

The default value is \c true.
- \c can_grab (boolean): \c true to allow to grab in this state.

\remark Ability \ref lua_api_game_get_ability "grab" is necessary so that
  the hero can grab obstacles.

\subsection lua_api_state_get_can_push state:get_can_push()

Returns whether the entity can try to push the obstacles it is facing.
- Return value (boolean): \c true if pushing is allowed in this state.

\subsection lua_api_state_set_can_push state:set_can_push(can_push)

Sets whether the entity can start pushing the obstacles it is facing.

If \c true, the entity will go to state \c "pushing" when it reaches
an obstacle and continues to move toward this obstacle for a configurable
\ref lua_api_state_get_pushing_delay "delay".
This does not mean that the obstacle being pushed will actually move
(\ref lua_api_block "blocks" can move when being pushed, but other entities
usually cannot).

The default value is \c true.
- \c can_push (boolean): \c true to allow to push in this state.

\remark Ability \ref lua_api_game_get_ability "push" is necessary so that
  the hero can push.

\subsection lua_api_state_get_pushing_delay state:get_pushing_delay()

Returns the delay before pushing when moving towards an obstacle during this
state.
- Return value (number): The pushing delay in milliseconds.

\subsection lua_api_state_set_pushing_delay state:set_pushing_delay(pushing_delay)

Sets the delay before pushing when moving towards an obstacle during this
state.

This only has an effect if
\ref lua_api_state_get_can_push "state:get_can_push()" is \c true.

The default value is \c 1000 ms.
- \c pushing_delay (number): The pushing delay in milliseconds
  (\c 0 for no delay).

\subsection lua_api_state_get_can_pick_treasure state:get_can_pick_treasure()

Returns whether \ref lua_api_pickable "pickable treasures" can be picked
during this state.
- Return value (boolean): \c true if pickable treasures can be picked
during this state.

\subsection lua_api_state_set_can_pick_treasure state:set_can_pick_treasure(can_pick_treasure)

Sets whether \ref lua_api_pickable "pickable treasures" can be picked
during this state.

The default value is \c true.
- \c can_pick_treasure (boolean): \c true to allow the entity to pick treasures
during this state.

\subsection lua_api_state_get_can_use_teletransporter state:get_can_use_teletransporter()

Returns whether the entity can take
\ref lua_api_teletransporter "teletransporters" during this state.
- Return value (boolean): \c true if the entity can take teletransporter
during this state.

\subsection lua_api_state_set_can_use_teletransporter state:set_can_use_teletransporter(can_use_teletransporter)

Sets whether the entity can take
\ref lua_api_teletransporter "teletransporters"
during this state.

The default value is \c true.
- \c can_use_teletransporter (boolean): \c true to allow the entity to take
teletransporters during this state.

\subsection lua_api_state_get_can_use_switch state:get_can_use_switch()

Returns whether the entity can activate
\ref lua_api_switch "switches" during this state.
- Return value (boolean): \c true if the entity can activate switches
during this state.

\subsection lua_api_state_set_can_use_switch state:set_can_use_switch(can_use_switch)

Sets whether the entity can activate
\ref lua_api_switch "switches"
during this state.

The default value is \c true.
- \c can_use_switch (boolean): \c true to allow the entity to activate
switches during this state.

\subsection lua_api_state_get_can_use_stream state:get_can_use_stream()

Returns whether the entity can take
\ref lua_api_stream "streams" during this state.
- Return value (boolean): \c true if the entity can take stream
during this state.

\subsection lua_api_state_set_can_use_stream state:set_can_use_stream(can_use_stream)

Sets whether the entity can take
\ref lua_api_stream "streams"
during this state.

The default value is \c true.
- \c can_use_stream (boolean): \c true to allow the entity to take
streams during this state.

\subsection lua_api_state_get_can_use_stairs state:get_can_use_stairs()

Returns whether the entity can take \ref lua_api_stairs "stairs"
during this state.
- Return value (boolean): \c true if the entity can take stairs
during this state.

\subsection lua_api_state_set_can_use_stairs state:set_can_use_stairs(can_use_stairs)

Sets whether the entity can take \ref lua_api_stairs "stairs"
during this state.

The default value is \c true.
- \c can_use_stairs (boolean): \c true to allow the entity to take stairs
during this state.

\subsection lua_api_state_get_can_use_jumper state:get_can_use_jumper()

Returns whether the entity can activate \ref lua_api_jumper "jumpers"
during this state.
- Return value (boolean): \c true if the entity can take jumpers
during this state.

\subsection lua_api_state_set_can_use_jumper state:set_can_use_jumper(can_use_jumper)

Sets whether the entity can take \ref lua_api_jumper "jumpers"
during this state.

The default value is \c true.
- \c can_use_jumper (boolean): \c true to allow the entity to take jumpers
during this state.

\subsection lua_api_state_get_jumper_delay state:get_jumper_delay()

Returns the delay before jumping when taking a jumper during this state.
- Return value (number): The jump delay in milliseconds
  (\c 0 means no delay).

\subsection lua_api_state_set_jumper_delay state:set_jumper_delay(jumper_delay)

Sets the delay before jumping when taking a jumper during this state.

This only has an effect if
\ref lua_api_state_get_can_use_jumper "state:get_can_use_jumper()" is \c true.

The default value is \c 200 ms.
- \c jumper_delay (number): The jump delay in milliseconds
  (\c 0 for no delay).

\subsection lua_api_get_carried_object_reaction state:get_carried_object_action()

Returns what happens during this state to an object
that was \ref lua_api_carried_object "carried"
the previous state.
- Return value (string): One of:
  - \c "throw" (default): The carried object is automatically thrown.
  - \c "remove": The carried object is silently destroyed.
  - \c "keep": The carried object continues to be carried.

\subsection lua_api_set_carried_object_action state:set_carried_object_action(action)

Sets what happens during this state to an object
that was \ref lua_api_carried_object "carried"
the previous state.
- \c action (string): One of:
  - \c "throw" (default): The carried object is automatically thrown.
  - \c "remove": The carried object is silently destroyed.
  - \c "keep": The carried object continues to be carried.

\section lua_api_state_events Events of a state

Events are callback methods automatically called by the engine if you define
them.
In the case of states, they are only called on an active state.

\subsection lua_api_state_on_started state:on_started(previous_state_name, previous_state)

Called when this state starts.
- \c previous_state_name (string or nil): Name of the state that was active before.
  See \ref lua_api_hero_get_state "hero:get_state()" for the possible values.
  This value is \c nil if there was no state before
  (only possible for the first state of an entity).
- \c previous_state (state or nil): Custom state object that was active before,
  if it was a custom one, \c nil otherwise.

\subsection lua_api_state_on_finished state:on_finished(next_state_name, next_state)

Called when this state finishes.
- \c next_state_name (string or nil): Name of the state that is about to be
  active after yours.
  See \ref lua_api_hero_get_state "hero:get_state()" for the possible values.
  This value is \c nil if there is no state after yours
  (only possible if the entity is being removed).
- \c next_state (state or nil): Custom state object about to start,
  if it is a custom one, \c nil otherwise.

\subsection lua_api_state_on_update state:on_update()

Called at each cycle of the main loop while this state is active.

\remark As this function is called at each cycle, it is recommended to use other
solutions when possible, like \ref lua_api_timer "timers" and other events.

\subsection lua_api_state_on_pre_draw state:on_pre_draw(camera)

Called just before the entity is drawn on the map during this state.

You may display additional things below the entity.
To do so, you can either call
\ref lua_api_map_draw_visual "map:draw_visual()"
or draw on \ref lua_api_camera "camera:get_surface()".
- \c camera (\ref lua_api_camera "camera"): The camera where this entity is
  being drawn.

\subsection lua_api_state_on_post_draw state:on_post_draw(camera)

Called just after the entity is drawn on the map during this state.

You may display additional things above the entity.
To do so, you can either call
\ref lua_api_map_draw_visual "map:draw_visual()"
or draw on \ref lua_api_camera "camera:get_surface()".
- \c camera (\ref lua_api_camera "camera"): The camera where this entity is
  being drawn.

\subsection lua_api_state_on_suspended state:on_suspended(suspended)

Called when the \ref lua_api_entity "entity"
has just been suspended or resumed.

The entity is suspended by the engine in a few cases, like when the \ref
lua_api_game "game" is
paused or when a dialog is active.
When this happens, all \ref lua_api_entity "map entities" stop moving and most
\ref lua_api_sprite "sprites" stop their animation.
- \c suspended (boolean): \c true if the entity was just suspended,
  \c false if it was resumed.

\subsection lua_api_state_on_map_started state:on_map_started(map, destination)

Called when a map starts (when the player enters it) during this state.
- \c map (\ref lua_api_map "map"): The new map.
- \c destination (\ref lua_api_destination "destination"): The destination
  entity from where the \ref lua_api_hero "hero" arrives on the map,
  or \c nil if he used another way than a destination entity
  (like the side of the map or direct coordinates).

\subsection lua_api_state_on_map_finished state:on_map_finished()

Called when the map stops (when the player leaves it) during this state.

\subsection lua_api_state_on_map_opening_transition_finished state:on_map_opening_transition_finished(map, destination)

When a map begins during this state,
called when the opening transition effect finishes.
- \c map (\ref lua_api_map "map"): The map.
- \c destination (\ref lua_api_destination "destination"): The destination
  entity from where the \ref lua_api_hero "hero" arrived on the map,
  or \c nil if he used another way than a destination entity
  (like the side of the map or direct coordinates).

\subsection lua_api_state_on_position_changed state:on_position_changed(x, y, layer)

Called when the coordinates of the entity controlled by this state
have just changed.
- \c x (number): The new X coordinate of the entity.
- \c y (number): The new Y coordinate of the entity.
- \c layer (number): The new layer of the entity.

\subsection lua_api_state_on_ground_below_changed state:on_ground_below_changed(ground_below)

Called when the kind of
\ref lua_api_map_get_ground "ground" on the map below the entity controlled
by this state has changed.
It may change because the entity is moving,
or when because another entity changes it.
- \c ground_below (string): The kind of ground at the
  \ref lua_api_entity_get_ground_position "ground point"
  of the entity controlled by this state.
  \c nil means empty, that is, there is no ground at this point on the
  current layer.

\subsection lua_api_state_on_obstacle_reached state:on_obstacle_reached()

Called when the \ref lua_api_movement "movement"
of the entity was stopped because of an obstacle during this state.

When an obstacle is reached, this event is called instead of
\ref lua_api_state_on_position_changed "state:on_position_changed()".
- \c movement (\ref lua_api_movement "movement"): The movement of the entity.

\subsection lua_api_state_on_movement_started state:on_movement_started(movement)

Called when a
\ref lua_api_movement "movement"
is started on the entity controlled by this state.
- \c movement (\ref lua_api_movement "movement"): The movement that was just
  started on the entity.

\subsection lua_api_state_on_movement_changed state:on_movement_changed(movement)

Called when some characteristics of the entity's
\ref lua_api_movement "movement"
(like the speed or the angle) have just changed during this state.
- \c movement (\ref lua_api_movement "movement"): The movement of the entity.

\subsection lua_api_state_on_movement_finished state:on_movement_finished()

Called when the
\ref lua_api_movement "movement"
of the entity controlled by this state is finished (if there is an end).

\subsection lua_api_state_on_attacked_enemy state:on_attacked_enemy(enemy, enemy_sprite, attack, consequence)

Called when the entity has just attacked an enemy during this state,
even if the attack was not successful.
- \c enemy (\ref lua_api_enemy "enemy"): The attacked enemy.
- \c enemy_sprite (\ref lua_api_sprite "sprite"): Sprite of the enemy that
  received the attack, or \c nil if the attack does not come from a
  pixel-precise collision test.
- \c attack (string): How the enemy was attacked.
  See \ref lua_api_enemy_set_attack_consequence "enemy:set_attack_consequence()"
  for the possible values.
- \c consequence (number, string or function): How the enemy reacted to the
  attack.
  See \ref lua_api_enemy_set_attack_consequence "enemy:set_attack_consequence()"
  for the possible values.

\subsection lua_api_state_on_command_pressed state:on_command_pressed(command)

Called when the player presses a \ref lua_api_game_overview_commands
"game command"
(a keyboard key or a joypad action mapped to a built-in game behavior)
during this state.
You can use this event to override the normal built-in behavior of the game
command.
- \c command (string): Name of the built-in game command that was pressed.
  Possible commands are
  \c "action", \c "attack", \c "pause", \c "item_1", \c "item_2",
  \c "right", \c "up", \c "left" and \c "down".
- Return value (boolean): Indicates whether the event was handled. If you
  return \c true, the event won't be propagated to other objects
  (you are overriding the built-in behavior of pressing this game command).

\remark This event is not triggered if you already handled its underlying
  low-level keyboard or joypad event.

\subsection lua_api_state_on_command_released state:on_command_released(command)

Called when the player released a \ref lua_api_game_overview_commands
"game command"
(a keyboard key or a joypad action mapped to a built-in game behavior).
during this state.
You can use this event to override the normal built-in behavior of the game
command.
- \c command (string): Name of the built-in game command that was released.
  Possible commands are
  \c "action", \c "attack", \c "pause", \c "item_1", \c "item_2",
  \c "right", \c "up", \c "left" and \c "down".
- Return value (boolean): Indicates whether the event was handled. If you
  return \c true, the event won't be propagated to other objects
  (you are overriding the built-in behavior of releasing this game command).

\remark This event is not triggered if you already handled its underlying
  low-level keyboard or joypad event.

\section lua_api_state_input_handler_events Events as an Input Handler

In addition to its own events a state is also an input handler and receives
all of those events.

The hero's current state is checked after the current map. Only its menus
and the conversion to a \ref lua_api_game_overview_commands "game command"
follow.

See \ref lua_api_input_handler_events to see all of these events.

*/
